const initialState =[
  {
    login:"Scott",
    password:"123",
    name:"Petya"
  },
  {
    login:"Lil Yachty",
    password:"1234",
    name:"Vasya"
  },
  {
    login:"Rakim",
    password:"12345",
    name:"Anton"
  },
  {
    login:"Lil Atty",
    password:"123456",
    name:"Atanuyaz"
  }
];

export default function users (state=initialState, action){
  if(action.type === 'ADD_USER'){
    return[
      ...state,
      action.payload
    ];
  }
  return state;
}
