const initialState =[
  {
    id:1,
    offices:[
      {
        id:11,
        name:"Ximki",
        address:"Ximki",
        phone:12315649,
        isActive:true
      },
      {
        id:12,
        name:"Kreml",
        address:"Kreml",
        phone:1231521549,
        isActive:true
      },
      {
        id:13,
        name:"China",
        address:"China town",
        phone:12377777649,
        isActive:true
      }
    ]
  },
  {
    id:2,
    offices:[
      {
        id:21,
        name:"San Antonio",
        address:"San Antonio",
        phone:11765649,
        isActive:true
      },
      {
        id:22,
        name:"San Diego",
        address:"San Diego",
        phone:124219891549,
        isActive:true
      },
      {
        id:23,
        name:"Compton",
        address:"Compton",
        phone:1243537649,
        isActive:true
      }
    ]
  },
  {
    id:3,
    offices:[
      {
        id:31,
        name:"Urypinsk",
        address:"Urypinsk",
        phone:121655649,
        isActive:true
      },
      {
        id:32,
        name:"Volgograd",
        address:"Volgograd",
        phone:12352156549,
        isActive:true
      },
      {
        id:33,
        name:"Bubnovka",
        address:"Bubnovka",
        phone:12377649,
        isActive:true
      }
    ]
  }
]

export default function officesData(state=initialState,action){
  if(action.type === "ADD_OFFICE"){
    return[
      ...state,
      action.payload
    ];
  }
  return state;
}
