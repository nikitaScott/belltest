import { combineReducers } from 'redux';
import { routerReducer } from "react-router-redux";

import users from "./users";
import organizations from "./organizations";
import officesData from './offices';


export default combineReducers({
  router: routerReducer,
  users,
  organizations,
  officesData
})
