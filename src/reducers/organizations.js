const initialState =[
  {
    id:1,
    name:"MMM",
    fullName:"MMM",
    inn:12345678,
    kpp:123,
    address:"magadan",
    phone:8800553535,
    isActive:true
  },
  {
    id:2,
    name:"LAConfidential",
    fullName:"LAConfidential",
    inn:12345,
    kpp:13,
    address:"los-angeles",
    phone:180080080,
    isActive:true
  },
  {
    id:3,
    name:"RosAtom",
    fullName:"Russian Atom",
    inn:123476865,
    kpp:133,
    address:"moscow",
    phone:8495435435,
    isActive:true
  }
]

export default function organizations(state=initialState,action){
  if(action.type === "ADD_ORG"){
    return[
      ...state,
      action.payload
    ];
  }
  return state;
}
