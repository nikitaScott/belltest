import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import "./index.less";


class Office extends React.Component {
  constructor(props){
    super(props)
  };
  render(){
     console.log("a", this.props.off);
  //  const of =this.props.off.offices.find(off => off.id === Number(this.props.officeId));
    return(
      <div className="grid">
        <div className="card__back-btn">
          <Link to={`/offices/${this.props.officeLog}`}>
            <button
              className="btn btn-outline-secondary">
                Назад
            </button>
          </Link>
        </div>
        <div className="card card__organization ">

            <div className="card-header"> {this.props.off.name} </div>


            <div className="card-body">
              <h5 className="card-title">Адресс</h5>
              <p className="">{this.props.off.address}</p>
            </div>
            <div className="card-body">
              <h5 className="card-title">Телефон</h5>
              <p className="">{this.props.off.phone}</p>
            </div>
        </div>
      </div>
    )
  }
}
const mapStateToProps =(state, ownProps) =>{
   console.log(ownProps);
  return{
    off:state.officesData.find(off => off.id === Number(ownProps.match.params.log)).offices.find(off => off.id === Number(ownProps.match.params.id)),
    officeLog:ownProps.match.params.log
  }
};

export default connect(mapStateToProps)(Office);
