import React from 'react';
import "./index.less";
import { Link } from 'react-router-dom';
import { connect } from "react-redux";

class OfficesList extends React.Component {
  constructor(props){
    super(props)
  };
  render(){

    return(
      <div className="grid">
        <div className="back-btn">
          <Link to="/organizations">
            <button
              className="btn btn-outline-secondary">
                Назад
            </button>
          </Link>
        </div>
        <table className="table  table-bordered table-hover">
          <thead>
            <tr>
              <th>Офиссы</th>
            </tr>
          </thead>
          <tbody>
                {this.props.off.offices.map(off =>
                  <tr key={off.id}>
                    <td >
                      <div className="table__line" >
                        <Link to={`/office/${this.props.log}/${off.id}`}>
                          <div>{off.name}</div>
                        </Link>
                        <div className="table__line-buttons">
                          <button className=" btn btn-outline-success">
                            Сотрудники
                          </button>
                          <button className=" btn btn-outline-info">
                            Изменить
                          </button>
                          <button className=" btn btn-outline-danger">
                            Удалить
                          </button>
                        </div>
                      </div>
                    </td>
                  </tr>
                )}
          </tbody>
        </table>
      </div>
    )
  }
}

const mapStateToProps=(state, ownProps) =>{

  return{
    off:state.officesData.find(off => off.id === Number(ownProps.match.params.id)),
    log:ownProps.match.params.id,
  }
}

export default connect(mapStateToProps)(OfficesList);
