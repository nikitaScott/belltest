import React from 'react';
import './index.less';
import { Link } from 'react-router-dom';
import { connect } from "react-redux";
import DeleteForm from '../DeleteForm';


class OrganizationList extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      del:false
    }
  };
  render(){

    let delForm;
    if(this.state.del){
      delForm=<DeleteForm click={this.onReturnDeleteClick} />;
      //console.log("a");
    }

    return(
      <div className="grid">
        <div className="singOut-btn">
          <Link to="/login">
            <button
              className="btn btn-outline-secondary">
                Выход
            </button>
          </Link>
        </div>
        {delForm}
        <table className="table  table-bordered table-hover">
          <thead>
            <tr>
              <th>Организации</th>
            </tr>
          </thead>
          <tbody>
                {this.props.orgs.map(org =>
                  <tr key={org.id}>
                    <td >
                      <div className="table__line" >
                        <div>
                          <Link to={`/organizations/${org.id}`}>
                            {org.name}
                          </Link>
                        </div>
                        <div className="table__line-buttons">
                          <Link to={`/offices/${org.id}`}>
                            <button className=" btn btn-outline-success">
                              Оффисы
                            </button>
                          </Link>
                          <Link to={`/organizations/update/${org.id}`}>
                            <button className=" btn btn-outline-info">
                              Изменить
                            </button>
                          </Link>
                          <button
                            className=" btn btn-outline-danger"
                            onClick={this.onDeleteClick}>
                            Удалить
                          </button>
                        </div>
                      </div>
                    </td>
                  </tr>
                )}
          </tbody>
        </table>
      </div>
    )
  }

  onDeleteClick =()=>{
    //console.log("assa");
    this.setState({
      del:true
    })
  }

  onReturnDeleteClick = ()=>{
    this.setState({
      del:false
    })
  }
}
export default connect (
 state =>({
   orgs:state.organizations
 })
)(OrganizationList);
