import React from 'react';
import './index.less';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

function ErrorView(props){
  if(!props.view){
    return null
  }
  return(
    <button
      type="button"
      className="btn btn-danger">
      Вы не зарегистрированны
    </button>
  )
}

function SuccessView(props){
  if(!props.view){
    return null
  }
  return(
    <button
      type="button"
      className="btn btn-success ">
      С возвращением дорогой
    </button>
  )
}

class SignInForm extends React.Component{
  constructor(props){
    super(props);
    this.state ={
      error:false,
      login:"",
      password:""
    }
  };
  render(){
    let user = this.props.users.find(user =>(user.login === String(this.state.login)) &&
      (user.password === String(this.state.password) ));

      let linkTo=`/login`;
      if(user){
        linkTo=`/organizations`;
      }

    return(
      <div className="grid">
        <form className="form-gorizontal form__block">
          <div className="form-group">
            <label
              className="col-sm-2 control-label"
              htmlFor="login">
                Login
            </label>
            <div className="col-md-12">
              <input
                id="login"
                className="form-control"
                type="text"
                value={this.state.login}
                onChange={this.changeLogin} />
            </div>
          </div>
          <div className="form-group">
            <label
              className="col-sm-2 control-label"
              htmlFor="password">
                Password
            </label>
            <div className="col-md-12">
              <input
                id="password"
                className="form-control"
                type="password"
                value={this.state.password}
                onChange={this.changePassword}/>
            </div>
          </div>
          <div className="form-group">
            <div className="col-sm-offset-4 col-md-12">
              <Link to={linkTo}>
                <button
                  type="submit"
                  className="btn btn-default col-md-12"
                  onClick={this.enter}>
                  Войти
                </button>
              </Link>
            </div>
          </div>
          <div className="form-group">
            <Link to="/register">
              <div className="col-sm-offset-4 col-md-12">
                <button
                  type="submit"
                  className="btn btn-default col-md-12">
                  Зарегистрироваться
                </button>
              </div>
            </Link>
          </div>
        </form>
        <div className="form-group ">
          <ErrorView
            view={this.state.error}
            className="col-md-12"/>
        </div>
        <div className="form-group ">
          <SuccessView
            view={this.state.success}
             className="col-md-12"/>
        </div>
      </div>
    )
  }

  changeLogin =(e) =>{
    this.setState({
      login:e.target.value
    })
  }

  changePassword =(e) =>{
    this.setState({
      password:e.target.value
    })
  }

  componentDidMount =()=>{

    let user = this.props.users.find(user =>(user.login === String(this.state.value)) &&
      (user.password === String(this.state.value) ));
      console.log("user", user);
  }

  enter =() =>{
    console.log(this.props.users);
    let user = this.props.users.find(user =>(user.login === String(this.state.value)) &&
      (user.password === String(this.state.value) ));
      console.log("user", user);
    if(!user){
      this.setState({
        error:true
      })
      setTimeout(() => {
        this.setState({
          error:false
        })
      },2000)
    }
  }
}

export default connect(
  state =>({
    users:state.users
  })
)(SignInForm);
