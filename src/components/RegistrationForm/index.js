import React from "react";
import './index.less';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';


function SuccessView(props){
  if(!props.view){
    return null
  }
  return(
    <button
      type="button"
      className="btn btn-success ">
      Добро пожаловать
    </button>
  )
}

function ErrorView(props){
  if(!props.view){
    return null
  }
  return(
    <button
      type="button"
      className="btn btn-danger">
      Необходимо заполнить все поля
    </button>
  )
}

class RegistrationForm extends React.Component{
  constructor(props){
    super(props)
    this.state={
      success:false,
      error:false
    }
  };

  render(){

    return(
      <div className="grid">
        <form className="form-gorizontal form__block">
          <div className="form-group">
            <label
              className="col-sm-2 control-label"
              htmlFor="login">
                Login
            </label>
            <div className="col-md-12">
              <input
                id="login"
                className="form-control"
                type="text"
                ref={ (input) => { this.loginInput = input } } />
            </div>
          </div>
          <div className="form-group">
            <label
              className="col-sm-2 control-label"
              htmlFor="password">
                Password
            </label>
            <div className="col-md-12">
              <input
                id="password"
                className="form-control"
                type="password"
                ref={ (input) => { this.passwordInput = input } }/>
            </div>
            <div className="form-group">
              <label
                className="col-sm-2 control-label"
                htmlFor="name">
                  Name
              </label>
              <div className="col-md-12">
                <input
                  id="name"
                  className="form-control "
                  type="text"
                  ref={ (input) => { this.nameInput = input } } />
              </div>
            </div>
          </div>
          <div className="form-group">

            <div className="col-sm-offset-4 col-md-12">
              <button
                type="submit"
                className="btn btn-default col-md-12"
                onClick={this.addUser}>
                Зарегистрироваться
              </button>
            </div>
          </div>
          <div className="form-group">
            <div className="col-sm-offset-4 col-md-12 ">
              <Link to="/login">
                 <button
                   type="submit"
                   className="btn btn-default">
                   У меня есть учетная запись
                 </button>
              </Link>
            </div>
          </div>
        </form>
        <div className="form-group ">
          <SuccessView view={this.state.success} className="col-md-12"/>
        </div>
        <div className="form-group ">
          <ErrorView view={this.state.error} className="col-md-12"/>
        </div>
      </div>
    )
  }

  addUser = () =>{
    if(this.loginInput.value &&
      this.passwordInput.value &&
      this.nameInput.value){
        this.props.onAddUser(this.loginInput.value, this.passwordInput.value,  this.nameInput.value);
        this.loginInput.value="";
        this.passwordInput.value="";
        this.nameInput.value="";

        this.setState({
          success:true
        })

        setTimeout(() => {
          this.setState({
            success:false
          })
          },2000)

    }else if(this.loginInput.value ||
      this.passwordInput.value ||
      this.nameInput.value){

        this.setState({
          error:true
        })

        setTimeout(() => {
          this.setState({
            error:false
          })
        },2000)
      }

  }
}

export default connect(
  state =>({
    users:state.users
  }),
  dispatch => ({
    onAddUser:(login,password,name) => {
      const payload ={
        login,
        password,
        name
      };
      dispatch({type:"ADD_USER",payload});
    }
  })
)(RegistrationForm);
