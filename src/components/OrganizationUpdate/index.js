import React from 'react';
import './index.less';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

function SuccessView(props){
  if(!props.view){
    return null
  }
  return(
    <button
      type="button"
      className="btn btn-success ">
      Изменения внесены
    </button>
  )
}

class OrganizationUpdate extends React.Component{
  constructor(props){
    super(props);
    this.state={
      success:false
    }
  };
  render(){
    console.log("org", this.props.organization);
    return(
      <div className="grid ">
        <div className="card__back-btn">
          <Link to="/organizations">
            <button
              className="btn btn-outline-secondary">
                Назад
            </button>
          </Link>
        </div>
        <div className="card card__organization">
          <div className="card-header">
            {this.props.organization.name}
          </div>
          <div className="card-body">
            <form>
              <div className="form-group">
                <label htmlFor="name">Имя</label>
                <input
                  type="text"
                  id="name"
                  className="form-control"
                  placeholder="Введите Имя"
                  ref={ (input) => { this.nameInput = input } }/>
              </div>
              <div className="form-group">
                <label htmlFor="fullName">Полное имя</label>
                <input
                  type="text"
                  id="fullName"
                  className="form-control"
                  placeholder="Введите полное имя"
                  ref={ (input) => { this.fullNameInput = input } }/>
              </div>
              <div className="form-group">
                <label htmlFor="inn">ИНН</label>
                <input
                  type="text"
                  id="inn"
                  className="form-control"
                  placeholder="Введите ИНН"
                  ref={ (input) => { this.innInput = input } }/>
              </div>
              <div className="form-group">
                <label htmlFor="kpp">КПП</label>
                <input
                  type="text"
                  id="kpp"
                  className="form-control"
                  placeholder="Введите КПП"
                  ref={ (input) => { this.kppInput = input } }/>
              </div>
              <div className="form-group">
                <label htmlFor="address">Адресс</label>
                <input
                  type="text"
                  id="address"
                  className="form-control"
                  placeholder="Введите Адресс"
                  ref={ (input) => { this.addressInput = input } }/>
              </div>
              <div className="form-group">
                <label htmlFor="phone">Телефон</label>
                <input
                  type="text"
                  id="phone"
                  className="form-control"
                  placeholder="Введите телефон"
                  ref={ (input) => { this.phoneInput = input } }/>
              </div>
              <button
                type="submit"
                className="btn btn-primary"
                onClick={this.submitData}>
                Изменить
              </button>
            </form>
          </div>
        </div>
        <div className="form-group success-btn">
          <SuccessView view={this.state.success} className=" col-md-12"/>
        </div>
      </div>
    )
  }

  submitData =() =>{
    this.setState({
      success:true
    })

    setTimeout(() => {
      this.setState({
        success:false
      })
      },2000)
  }
}

const mapStateToProps =(state,ownProps) =>{
  console.log(ownProps);

  return{
    organization:state.organizations.find(org => org.id === Number(ownProps.match.params.id)),
    //off:state.officesData.find(off => off.id === Number(ownProps.match.params.id))
  };
};

export default connect(mapStateToProps)(OrganizationUpdate);
