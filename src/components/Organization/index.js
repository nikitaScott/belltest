import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import "./index.less";


class Organization extends React.Component{
  constructor(props){
    super(props)
  }
  render(){
     //console.log("org", this.props.organization);
    // console.log("off",this.props.off);
    return(
      <div className="grid">
        <div className="card__back-btn">
          <Link to="/organizations">
            <button
              className="btn btn-outline-secondary">
                Назад
            </button>
          </Link>
        </div>
        <div className="card card__organization ">

            <div className="card-header"> {this.props.organization.name} </div>

            <div className="card-body">
              <h5 className="card-title">Полное имя</h5>
              <p className="">{this.props.organization.fullName}</p>
            </div>
            <div className="card-body">
              <h5 className="card-title">ИНН</h5>
              <p className="">{this.props.organization.inn}</p>
            </div>
            <div className="card-body">
              <h5 className="card-title">КПП</h5>
              <p className="">{this.props.organization.kpp}</p>
            </div>
            <div className="card-body">
              <h5 className="card-title">Адресс</h5>
              <p className="">{this.props.organization.address}</p>
            </div>
            <div className="card-body">
              <h5 className="card-title">Телефон</h5>
              <p className="">{this.props.organization.phone}</p>
            </div>
        </div>
      </div>


    )
  }
};

const mapStateToProps = (state, ownProps) =>{
  //console.log(ownProps);
  return{
    organization:state.organizations.find(org => org.id === Number(ownProps.match.params.id))
    //off:state.officesData.find(off => off.id === Number(ownProps.match.params.id))
  };
};

export default connect(mapStateToProps)(Organization);
