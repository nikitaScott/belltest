import React from 'react';
import './index.less';

class DeleteForm extends React.Component{
  constructor(props){
    super(props)
  }
  render(){
    return(
      <div className="delete-form">
          <div className="card">
            <div className="card-body">
              <div className='card-title'>
                Желаете удалить организацию?
              </div>
                <div className="card-body button-block">

                  <button
                    className="btn btn-danger"
                    onClick={this.props.click}>
                    Удалить
                  </button>

                  <button
                    className="btn btn-secondary"
                    onClick={this.props.click}>
                    Отмена
                  </button>
                </div>
            </div>
          </div>
      </div>
    )
  }
}

export default DeleteForm;
