import React from 'react';
import ReactDom from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware} from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from "redux-thunk";
import { Switch } from 'react-router-dom';
import {  Route } from 'react-router-dom';
import {ConnectedRouter, routerMiddleware} from "react-router-redux";
import createHistory from 'history/createHashHistory';


import '../node_modules/bootstrap/dist/css/bootstrap.css';

import RegistrationForm from "./components/RegistrationForm";
import SignInForm from "./components/SignInForm";
import OrganizationsList from "./components/OrganizationsList";
import Organization from './components/Organization';
import OfficesList from './components/OfficesList';
import Office from "./components/Office";
import OrganizationUpdate from "./components/OrganizationUpdate";


import reducers from './reducers';

const history = createHistory();

const middleware = routerMiddleware(history);

const store = createStore(reducers, composeWithDevTools(applyMiddleware(thunk, middleware)));


ReactDom.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <div>
        <Route path="/register" component={RegistrationForm} />
        <Route path="/login" component={SignInForm} />
        <Switch>
          <Route  exact path="/organizations" component={OrganizationsList} />
          <Route  exact path="/organizations/update/:id" component={OrganizationUpdate}/>
          <Route  exact path="/organizations/:id" component={Organization}/>
        </Switch>
        <Route path="/offices/:id" component={OfficesList}/>
        <Route path="/office/:log/:id" component={Office} />
      </div>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);
